import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'tsidta.example.app',
  appName: 'groupe15',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
