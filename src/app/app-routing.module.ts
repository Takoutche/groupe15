import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'onglets/onglets/accueil',
    pathMatch: 'full'
  },
  {
    path: 'accueil',
    loadChildren: () => import('./views/accueil/accueil.module').then( m => m.AccueilPageModule)
  },
  {
    path: 'partager',
    loadChildren: () => import('./views/partager/partager.module').then( m => m.PartagerPageModule)
  },
  {
    path: 'bluetooth',
    loadChildren: () => import('./views/bluetooth/bluetooth.module').then( m => m.BluetoothPageModule)
  },
  {
    path: 'appareil',
    loadChildren: () => import('./views/appareil/appareil.module').then( m => m.AppareilPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./views/about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'onglets',
    loadChildren: () => import('./views/onglets/onglets.module').then( m => m.OngletsPageModule)
  },
  {
    path: 'partager',
    loadChildren: () => import('./views/partager/partager.module').then( m => m.PartagerPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
