import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BleClient, BluetoothLe } from '@capacitor-community/bluetooth-le';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-bluetooth',
  templateUrl: './bluetooth.page.html',
  styleUrls: ['./bluetooth.page.scss'],
})
export class BluetoothPage implements OnInit {

  devices: any[] = [];
  ble:boolean=false;
  scanText:string="";

  constructor(private change:ChangeDetectorRef, public alertController: AlertController) { }

  ngOnInit() {
    BleClient.initialize({ androidNeverForLocation: true }).then(()=>{
      BleClient.isEnabled().then((res)=>{
        if(res){
          this.ble = true;
        }else{
          this.ble = false; 
        }
      })
    })
  }
  toggleBle(event:any){
    if(this.ble){
      this.enableBluetooth();
    }else{
      this.disableBluetooth();
    }
  }

  enableBluetooth(){
    BleClient.enable();
  }

  disableBluetooth(){
    BleClient.disable();
  }

  startScanning(){
    this.scanText = "scan en cours...";
    BleClient.requestLEScan({allowDuplicates:false},(res1: any)=>{
      if(res1){
        console.log('Discovered ' + JSON.stringify(res1, null, 2));
        this.devices.push(res1);
        this.change.detectChanges();
      }
    })
    setTimeout(()=>{
      this.stopScanning();
    },20000)
    
  }
 
  stopScanning(){
    BluetoothLe.stopLEScan().then(()=>{
      this.scanText = "";
    })
  }

  async consulter(device:any){

    const alert = await this.alertController.create(
      {header: 'Informations',
      subHeader: device.name||'Sans Nom',
      message: 'UUIDS = '+device.uuids+', RSSI = '+device.rssi,
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => { console.log('Confirm Cancel: blah');}
        },{
          text: 'Okay',
          handler: () => { console.log('Confirm Okay');}
        }
      ]
    }
    );
  await alert.present();
  const { role } = await alert.onDidDismiss();
  console.log('onDidDismiss resolve with role', role);

  }

  // connect(device:any,index:any){
  //   BleClient.connect(device.device.deviceId).then(()=>{
  //     this.devices[index]["connexion"] = true;
  //     this.change.detectChanges();

  //     alert("connecter!");
  //   },(err)=>{
  //     alert(err);
  //   })
  // }

  // disconnect(device:any,index:any){
  //   BleClient.disconnect(device.device.deviceId).then(()=>{
  //     this.devices[index]["connexion"] = false;
  //     this.change.detectChanges();
  //     alert("Deconnexion!");
  //   })
  // }

}
