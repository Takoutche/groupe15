import { Component, OnInit } from '@angular/core';
import { Camera, CameraResultType } from '@capacitor/camera';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@Component({
  selector: 'app-partager',
  templateUrl: './partager.page.html',
  styleUrls: ['./partager.page.scss'],
})
export class PartagerPage implements OnInit {

  url:string="";

  constructor(private socialSharing: SocialSharing) { }

  ngOnInit() {
  }


  handleShare(imageUrl:string){
    this.socialSharing.share("Message","title",imageUrl, "url").
    then(res =>
      {console.log("Launched view!", res)
    }).catch(err => {
      console.log('Error launching view', err)
    });
  }

  async takePicture(){
    const image = await Camera.getPhoto({
      quality:90,
      allowEditing: true,
      resultType:CameraResultType.Uri
      
    });


    let imageUrl = image.webPath;

    // alert(JSON.stringify(image));
    if(imageUrl != undefined){
      this.url= imageUrl;
    }
  }

}
