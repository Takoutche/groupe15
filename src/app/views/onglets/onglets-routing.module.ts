import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OngletsPage } from './onglets.page';

const routes: Routes = [
  {
    path: 'onglets',
    component: OngletsPage
  },
  {
    path: 'onglets',
    component: OngletsPage,
    children:[
      {
        path: 'accueil',
        loadChildren: () => import('../accueil/accueil.module').then( m => m.AccueilPageModule)
      },
      {
        path: 'accueil/partager',
        loadChildren: () => import('../partager/partager.module').then( m => m.PartagerPageModule)
      },
      {
        path: 'bluetooth',
        loadChildren: () => import('..//bluetooth/bluetooth.module').then( m => m.BluetoothPageModule)
      },
      {
        path: 'appareil',
        loadChildren: () => import('../appareil/appareil.module').then( m => m.AppareilPageModule)
      },
      {
        path: 'about',
        loadChildren: () => import('../about/about.module').then( m => m.AboutPageModule)
      },
      {
        path: 'accueil/home',
        loadChildren: () => import('../../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: '',
        redirectTo:'/onglets/home',
        pathMatch:'full'
      }
    ]
  },
  {
    path: '',
    redirectTo:'/onglets/onglets/accueil',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OngletsPageRoutingModule {}
