
import { Component, OnInit } from '@angular/core';
import { Device } from '@capacitor/device';
// import { Plugins } from '@capacitor/core';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.page.html',
  styleUrls: ['./appareil.page.scss'],
})
export class AppareilPage implements OnInit {
  deviceName: string ='';
  deviceModel: string="";
  osVersion: string="";
  manufacturer: string="";
  platform: string="";
  deviceId: string=""; 
  batteryLevel!: number;
  isCharging!:boolean;
  usedMemory: any;
  freeDiskSpace: number | undefined;
  totalDiskSpace: number | undefined;
  constructor() {
    this.getDeviceInfo();
    this.getDeviceId();
    this.getBatteryInfo(); 
  }

  async getBatteryInfo() 
  {
    const info = await Device.getBatteryInfo();
    this.batteryLevel = Math.round(Number(info.batteryLevel)*100);
    this.isCharging = Boolean(info.isCharging);
  }

  async getDeviceInfo()
   {
    const info = await Device.getInfo();
    this.deviceName = info.name ?? 'Non disponible';
    this.deviceModel = info.model ?? 'Non disponible';
    this.osVersion = info.osVersion ?? 'Non disponible';
    this.manufacturer = info.manufacturer ?? 'Non disponible';
    this.platform = info.platform ?? 'Non disponible';
    this.usedMemory = Math.round(Number(info.realDiskTotal)/(1024 * 1024 * 1024)) - Math.round(Number(info.realDiskFree) / (1024 * 1024 * 1024));
    this.freeDiskSpace = Math.round(Number(info.realDiskFree) / (1024 * 1024 * 1024));
    this.totalDiskSpace = Math.round(Number(info.realDiskTotal)/(1024 * 1024 * 1024));
    

  }

  async getDeviceId() {
    const info = await Device.getId();
    this.deviceId =  'Non disponible';
   // info.identifier ??
  }

  ngOnInit() {
  }
}
